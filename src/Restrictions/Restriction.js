export class Restriction {
    constructor(difficulty, speed, id) {
        this.difficulty = difficulty;
        this.speed = speed;
        this.id = id;
    }
}