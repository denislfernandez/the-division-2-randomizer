import {getRandomValues, Restriction} from './Utils.js';

const singleTypeWeapon = new Restriction(4, 2, "single-type-weapon")
const dualTypeWeapon = new Restriction(3, 3, "dual-type-weapon")

const weaponTypes = {
    AR: "ar", 
    SMG: "smg", 
    LMG: "lmg", 
    SNIPER: "sniper", 
    MARKSMAN: "marksman", 
    SHOTGUN: "shotgun"
}

const availableWeaponTypes = [
    weaponTypes.AR, weaponTypes.SMG, weaponTypes.LMG, weaponTypes.SNIPER, weaponTypes.MARKSMAN, weaponTypes.SHOTGUN
]

export const typeRestrictions = [singleTypeWeapon, dualTypeWeapon]

export function getRandomWeaponTypes(typeRestriction) {
    getRandomValues(typeRestrictions, 1)
    var restrictionSize = 1
    switch (typeRestriction.id) {
        case "single-type-weapon":
            restrictionSize = 1
            break;
        case "dual-type-weapon":
            restrictionSize = 2
            break;
    }
    return getRandomValues(availableWeaponTypes, restrictionSize)
}

const noModsAllowed = new Restriction(4, 1, "no-mods-allowed")
const singleModTypeAllowed = new Restriction(3, 2, "single-mod-type-allowed")

export const modRestrictions = [noModsAllowed, singleModTypeAllowed]