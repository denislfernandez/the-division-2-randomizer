import './App.css';
import React from 'react'
import FormTest from './Form.js';
import * as weapons from './Weapons.js';
import { Form } from 'react-bootstrap';

class App extends React.Component {
    render() {
        randomizeRestrictions()
        return <FormTest />
    }
}

export default App;

function randomizeRestrictions() {
    var weaponType = weapons.getRandomWeaponTypes()
    console.log(weaponType)
}