import {Restriction} from './Utils.js';

const singleTypeWeapon = new Restriction(4, 2, "single-type-weapon")
const dualTypeWeapon = new Restriction(3, 3, "dual-type-weapon")

const weaponTypes = {
    AR, SMG, LMG, SNIPER, RIFLE, SHOTGUN
}

export const typeRestrictions = [singleTypeWeapon, dualTypeWeapon]

export function getRandomWeaponTypes(typRestriction) {
    switch ()
}

const noModsAllowed = new Restriction(4, 1, "no-mods-allowed")
const singleModTypeAllowed = new Restriction(3, 2, "single-mod-type-allowed")

export const modRestrictions = [noModsAllowed, singleModTypeAllowed]