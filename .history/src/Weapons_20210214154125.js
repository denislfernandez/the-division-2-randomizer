import * as utils from './Utils.js';

const singleTypeWeapon = new utils.Restriction(4, 2, "single-type-weapon")
const dualTypeWeapon = new utils.Restriction(3, 3, "dual-type-weapon")

const weaponTypes = {
    AR: "ar",
    SMG: "smg",
    LMG: "lmg",
    MARKSMAN: "marksman",
    RIFLE: "rifle",
    SHOTGUN: "shotgun"
}

const availableWeaponTypes = [
    weaponTypes.AR, weaponTypes.SMG, weaponTypes.LMG, weaponTypes.MARKSMAN, weaponTypes.RIFLE, weaponTypes.SHOTGUN
]

export const typeRestrictions = [singleTypeWeapon, dualTypeWeapon]

export function getAllowedWeaponTypes(enabledRestrictions) {
    if (enabledRestrictions) {
        var restriction = utils.getRandomValue([...typeRestrictions])
        var restrictionSize = 1
        switch (restriction.id) {
            case singleTypeWeapon.id:
                restrictionSize = 1
                break;
            case dualTypeWeapon.id:
                restrictionSize = 2
                break;
        }
        return utils.getRandomValues([...availableWeaponTypes], restrictionSize)
    } else {
        return [...availableWeaponTypes]
    }
}

const singleNamedWeapon = new utils.Restriction(4, 2, "single-type-weapon")
const singleNamedWeaponWithPistol = new utils.Restriction(4, 2, "single-type-weapon")
const noNamedWeapons = new utils.Restriction(4, 2, "single-type-weapon")

const singleExoticWeapon = new utils.Restriction(4, 2, "single-type-weapon")
const singleExoticWeaponWithPistol = new utils.Restriction(4, 2, "single-type-weapon")
const noExoticWeapons = new utils.Restriction(4, 2, "single-type-weapon")