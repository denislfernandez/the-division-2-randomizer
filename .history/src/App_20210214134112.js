import logo from './logo.svg';
import './App.css';
import * as utils from './Utils.js';
import * as weapons from './Weapons.js';

function App() {
    randomizeRestrictions()
    return (
        <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <p>
                    Edit <code>src/App.js</code> and save to reload.
        </p>
                <a
                    className="App-link"
                    href="https://reactjs.org"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    Learn React
        </a>
            </header>
        </div>
    );
}

export default App;

function randomizeRestrictions() {
    var weaponType = utils.getRestrictions(weapons.typeRestrictions, 1)
    var mods = utils.getRestrictions(weapons.modRestrictions, 1)
    console.log(weaponType.id)
    console.log(mods.id)
}