import './App.css';
import React from 'react'
import FormTest from './Form.js';
import * as utils from './Utils.js';
import * as weapons from './Weapons.js';
class App extends React.Component {
    render() {
        randomizeRestrictions()
        return <FormTest />
    }
}

export default App;

function randomizeRestrictions() {
    var allowedWeapons = weapons.getAllowedWeaponTypes(utils.enabledRestriction())
    var allowedWeaponMods = weapons.getAllowedWeaponMods(utils.enabledRestriction())
    var namedWeapons = weapons.getNamedWeaponRestriction(utils.enabledRestriction())
    var exoticWeapons = weapons.getSignatureWeaponRestrictions(utils.enabledRestriction())
    var specGreandes = weapons.getSpecGrenadesRestrictions(utils.enabledRestriction())
    console.log(`Allowed weapon types: ${allowedWeapons}`)
    console.log(`Allowed weapon mods: ${allowedWeaponMods}`)
    if (namedWeapons != null) {
        console.log(`Allowed weapon mods: ${allowedWeaponMods}`)
    }
}