import './App.css';
import React from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Alert from 'react-bootstrap/Alert'
import 'bootstrap/dist/css/bootstrap.min.css';
import * as utils from './Utils.js';
import * as weapons from './Weapons.js';
import * as gear from './Gear.js';
// import i18n from "i18next";
// import { useTranslation, initReactI18next } from "react-i18next";

// i18n
//     .use(initReactI18next) // passes i18n down to react-i18next
//     .init({
//         resources: {
//             en: {
//                 translation: {
//                     "Welcome to React": "Welcome to React and react-i18next"
//                 }
//             }
//         },
//         lng: "en",
//         fallbackLng: "en",

//         interpolation: {
//             escapeValue: false
//         }
//     });
class App extends React.Component {
    render() {
        randomizeRestrictions()
        return <Container>
            <Row>
                <Col>
                    <Alert variant="success">
                        <Alert.Heading>Welcome to The Division 2 Randomizer!</Alert.Heading>
                        <p>
                            This tool is intended to be used when playing hardcore mode (or normal mode if you want a list of restrictions/objectives to be following)
                            to make each run you make a bit different. 
                        </p>
                        <hr></hr>
                        <p>
                            With the following options you can customize the restriction and objectives randomization.
                            When you have finished setting up the randomization click on the <strong>Randomize</strong> button and the results will be displayed in the final
                            text areas
                        </p>
                    
                    </Alert>
                    <Button onClick={randomizeRestrictions} variant="primary" size="lg" block>
                        Randomize
                    </Button>
                    <Form.Control id="restrictions" type="text" placeholder="Restrictions" readOnly />
                    <Form.Control id="objectives" type="text" placeholder="Objectives" readOnly />
                </Col>
            </Row>
        </Container>
    }
}

export default App;