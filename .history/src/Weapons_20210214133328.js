const singleTypeWeapon = new Restriction(4, 2, "single-type-weapon")
const dualTypeWeapon = new Restriction(3, 3, "dual-type-weapon")

const typeRestrictions = [singleTypeWeapon, dualTypeWeapon]

const noModsAllowed = new Restriction(4, 1, "no-mods-allowed")
const singleModTypeAllowed = new Restriction(3, 2, "single-mod-type-allowed")

const modRestrictions = [noModsAllowed, dualTypeWeapon]