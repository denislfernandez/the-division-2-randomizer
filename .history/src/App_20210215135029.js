import './App.css';
import React from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Alert from 'react-bootstrap/Alert'
import 'bootstrap/dist/css/bootstrap.min.css';
import * as utils from './Utils.js';
import * as weapons from './Weapons.js';
import * as gear from './Gear.js';
import i18n from "i18next";
import { withTranslation, useTranslation, initReactI18next } from "react-i18next";

i18n
    .use(initReactI18next) // passes i18n down to react-i18next
    .init({
        resources: {
            en: {
                translation: {
                    "single-named-weapon": "Only 1 named weapon allowed",
                    "single-named-weapon-pistol": "Only 1 named weapon allowed (+ named pistol)",
                    "no-named-weapon": "No named weapons allowed",
                    "single-exotic-weapon": "Only 1 exotic weapon allowed",
                    "single-exotic-weapon-pistol": "Only 1 exotic weapon allowed (+ exotic pistol)",
                    "no-exotic-weapon": "No exotic weapons allowed",
                    "no-signature-weapon": "No signature weapon allowed",
                    "no-spec-grenades": "No specialization grenades allowed",
                    "single-gear-type": "No specialization grenades allowed",
                    "no-gear-type": "No specialization grenades allowed",
                    "no-offensive-gear-mods": "No offensive gear mods allowed",
                    "no-defensive-gear-mods": "No defensive gear mods allowed",
                    "no-utility-gear-mods": "No utility gear mods allowed",
                    "only-offensive-gear-mods": "Only offensive gear mods allowed",
                    "only-defensive-gear-mods": "Only defensive gear mods allowed",
                    "only-utility-gear-mods": "Only utility gear mods allowed",
                    "no-gear-mods": "No gear mods allowed",
                    "no-gear-sets": "No gear sets allowed",
                    "no-named-gear": "No named gear allowed",
                    "single-named-gear-piece": "Single named gear piece allowed",
                    "no-exotic-gear": "No exotic gear allowed",
                    "single-exotic-gear-piece": "Single exotic gear piece allowed",
                    "single-gear-type": "Only gear with {{attr}} attribute allowed",
                    "no-gear-type": "No gear with {{attr}} attribute allowed",
                }
            }
        },
        lng: "en",
        fallbackLng: "en",
        interpolation: {
            escapeValue: false
        }
    });
class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            restrictions: "",
            restrictionsRows: 1,
            objectives: "",
            objectivesRows: 0,
        };  
    }

    handleRestrictionChange(event) {    
        this.setState({restrictions: event.target.value});  
    }

    handleObjectiveChange(event) {    
        this.setState({objectives: event.target.value});  
    }

    render() {
        return <Container>
            <Row>
                <Col>
                    <Alert variant="success">
                        <Alert.Heading>Welcome to The Division 2 Randomizer!</Alert.Heading>
                        <p>
                            This tool is intended to be used when playing hardcore mode (or normal mode if you want a list of restrictions/objectives to be following)
                            to make each run you make a bit different. 
                        </p>
                        <hr></hr>
                        <p>
                            With the following options you can customize the restriction and objectives randomization.
                            When you have finished setting up the randomization click on the <strong>Randomize</strong> button and the results will be displayed in the final
                            text areas
                        </p>
                    </Alert>
                    <Button onClick={this.randomizeRestrictions} variant="primary" size="lg" block>
                        Randomize
                    </Button>
                    <Form.Control as="textarea" rows={this.state.restrictionsRows + 2} id="restrictions" type="text" placeholder="Restrictions" value={this.state.restrictions} onChange={this.handleRestrictionChange} readOnly />
                    <Form.Control id="objectives" type="text" placeholder="Objectives" value={this.state.objectives} onChange={this.handleObjectiveChange} readOnly />
                </Col>
            </Row>
        </Container>
    }

    randomizeRestrictions = () => {
        const { t } = this.props;
        var restrictions = []
        var resRows = 0
        var gearList = []
        restrictions.push(
            `Weapon restrictions`,
        )
        resRows++
        if (utils.enabledRestriction()) {
            var weaponTypeRestriction = weapons.getWeaponTypeRestriction()
            var randomWeapons = weapons.getAllowedWeaponTypes(weaponTypeRestriction)
            restrictions.push(
                `   Allowed weapon types: ${randomWeapons}`,
            )
            resRows++
        }
        if (utils.enabledRestriction()) {
            var randomWeaponMods = weapons.getAllowedWeaponMods()
            if (randomWeaponMods.length == 0) {
                restrictions.push(
                    `   No weapon mods allowed`,
                )
            } else {
                restrictions.push(
                    `   Allowed weapon mods: ${randomWeaponMods}`,
                )
            }
            resRows++
        }
        if (utils.enabledRestriction()) {
            var randomNamedWeaponRestriction = weapons.getNamedWeaponRestriction()
            var text = t(randomNamedWeaponRestriction.id)
            restrictions.push(
               `   ${text}`,
            )
            resRows++
        }
        if (utils.enabledRestriction()) {
            var randomExoticWeaponRestriction = weapons.getExoticWeaponRestrictions()
            var text = t(randomExoticWeaponRestriction.id)
            restrictions.push(
               `   ${text}`,
            )
            resRows++
        }
        if (utils.enabledRestriction()) {
            var randomSingatureWeaponRestriction = weapons.getSignatureWeaponRestrictions()
            var text = t(randomSingatureWeaponRestriction.id)
            restrictions.push(
               `   ${text}`,
            )
            resRows++
        }
        if (utils.enabledRestriction()) {
            var randomSpecGrenadesRestriction = weapons.getSpecGrenadesRestrictions()
            var text = t(randomSpecGrenadesRestriction.id)
            restrictions.push(
               `   ${text}`,
            )
            resRows++
        }
        restrictions.push(``)
        resRows++
        if (utils.enabledRestriction()) {
            var randomAvailableGearRestriction = gear.getAvailableGearRestriction()
            var randomNonAvailableGear = gear.getNonAvailableGear(randomAvailableGearRestriction)
            restrictions.push(
                `Gear restrictions`,
                `   Banned gear: `,
            )
            resRows++
            resRows++
            for (var i = 0; i < randomNonAvailableGear.length; i++) {
                gearList.push(`     ${randomNonAvailableGear[i]}`)
                resRows++
            }
            restrictions.push(
                `${gearList.join(`\n`)}`
            )
        }
        if (utils.enabledRestriction()) {
            var randomGearAttributeRestriction = gear.getGearAttributeRestriction()
            var randomGearAttribueValue = gear.getGearAttributeValue()
            var text = t(randomGearAttributeRestriction.id, {attr: randomGearAttribueValue})
            restrictions.push(
               `   ${text}`,
            )
            resRows++
        }
        if (utils.enabledRestriction()) {
            var randomGearModRestriction = gear.getGearModRestriction()
            var text = t(randomGearModRestriction.id)
            restrictions.push(
               `   ${text}`,
            )
            resRows++
        }
        if (utils.enabledRestriction()) {
            var randomGearSetRestriction = gear.getGearSetRestriction()
            var text = t(randomGearSetRestriction.id)
            restrictions.push(
               `   ${text}`,
            )
            resRows++
        }
        if (utils.enabledRestriction()) {
            var randomNamedGearRestriction = gear.getNamedGearRestrictions()
            var text = t(randomNamedGearRestriction.id)
            restrictions.push(
               `   ${text}`,
            )
            resRows++
        }
        if (utils.enabledRestriction()) {
            var randomExoticGearRestriction = gear.getExoticGearRestrictions()
            var text = t(randomExoticGearRestriction.id)
            restrictions.push(
               `   ${text}`,
            )
            resRows++
        }
        return this.setState(state => ({
            restrictions: restrictions.join("\n"),
            restrictionsRows: resRows,
            objectives: "",
            objectivesRows: 0
        }));
    }
}

export default withTranslation()(App);