const singleTypeWeapon = new Restriction(4, 2, "single-type-weapon")
const dualTypeWeapon = new Restriction(3, 3, "dual-type-weapon")

const typeRestrictions = [singleTypeWeapon, dualTypeWeapon]