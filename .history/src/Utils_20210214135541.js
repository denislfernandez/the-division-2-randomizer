export class Restriction {
    constructor(difficulty, speed, id) {
        this.difficulty = difficulty;
        this.speed = speed;
        this.id = id;
    }
}

export function getRandomValues(array, numberOfValues) {
    var restrictions = []
    var maxValues = Math.min(numberOfValues, array.length)
    var processedArray = array
    for (var i = 0; i < maxValues; i++) {
        var index = Math.floor(Math.random() * processedArray.length)
        var restriction = processedArray[index]
        restrictions.push(restriction)
        processedArray = processedArray.splice(index, 1)
    }
    return restrictions;
}

export function getRandomValue(array, numberOfValues) {
    var restriction
    var maxValues = Math.min(numberOfValues, array.length)
    for (var i = 0; i < maxValues; i++) {
        var index = Math.floor(Math.random() * processedArray.length)
        var restriction = processedArray[index]
        restrictions.push(restriction)
        processedArray = processedArray.splice(index, 1)
    }
    return restrictions;
}