function getRestrictions(array, numberOfValues) {
    restrictions = []
    processedArray = array
    for (var i = 0; i < numberOfValues; i++) {
        index = Math.floor(Math.random() * processedArray.length)
        restriction = processedArray[index]
        restrictions.push(restriction)
        processedArray = processedArray.splice(index, 1)

    }
    return restrictions;
  }
  