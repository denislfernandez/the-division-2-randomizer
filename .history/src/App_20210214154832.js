import './App.css';
import React from 'react'
import FormTest from './Form.js';
import * as utils from './Utils.js';
import * as weapons from './Weapons.js';
import * as weaponMods from './WeaponMods.js';
class App extends React.Component {
    render() {
        randomizeRestrictions()
        return <FormTest />
    }
}

export default App;

function randomizeRestrictions() {
    var allowedWeapons = weapons.getAllowedWeaponTypes(utils.enabledRestriction())
    var allowedWeaponMods = weaponMods.getAllowedWeaponMods(utils.enabledRestriction())
    console.log(`Allowed weapon types: ${allowedWeapons}`)
    console.log(`Allowed weapon mods: ${allowedWeaponMods}`)
}