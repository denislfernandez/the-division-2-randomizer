import './App.css';
import React from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Alert from 'react-bootstrap/Alert'
import 'bootstrap/dist/css/bootstrap.min.css';
import * as utils from './Utils.js';
import * as weapons from './Weapons.js';
import * as gear from './Gear.js';
// import i18n from "i18next";
// import { useTranslation, initReactI18next } from "react-i18next";

// i18n
//     .use(initReactI18next) // passes i18n down to react-i18next
//     .init({
//         resources: {
//             en: {
//                 translation: {
//                     "Welcome to React": "Welcome to React and react-i18next"
//                 }
//             }
//         },
//         lng: "en",
//         fallbackLng: "en",

//         interpolation: {
//             escapeValue: false
//         }
//     });
class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            allowedWeapons: null,
            allowedWeaponMods: null,
            namedWeapons: null,
            exoticWeapons: null,
            signatureWeapons: null,
            specGreandes: null,
            availableGear: null,
            gearAttributeRestriction: null,
            gearAttributeValue: null,
            gearModRestriction: null,
            gearSetRestriction: null,
            namedGearRestriction: null,
            exoticGearRestriction: null,
            restrictions: "",
            objectives: ""
        };  
    }

    handleRestrictionChange(event) {    
        this.setState({restrictions: event.target.value});  
    }

    handleObjectiveChange(event) {    
        this.setState({objectives: event.target.value});  
    }

    render() {
        return <Container>
            <Row>
                <Col>
                    <Alert variant="success">
                        <Alert.Heading>Welcome to The Division 2 Randomizer!</Alert.Heading>
                        <p>
                            This tool is intended to be used when playing hardcore mode (or normal mode if you want a list of restrictions/objectives to be following)
                            to make each run you make a bit different. 
                        </p>
                        <hr></hr>
                        <p>
                            With the following options you can customize the restriction and objectives randomization.
                            When you have finished setting up the randomization click on the <strong>Randomize</strong> button and the results will be displayed in the final
                            text areas
                        </p>
                    </Alert>
                    <Button onClick={this.randomizeRestrictions} variant="primary" size="lg" block>
                        Randomize
                    </Button>
                    <Form.Control id="restrictions" type="text" placeholder="Restrictions" value={this.state.restrictions} onChange={this.handleRestrictionChange} readOnly />
                    <Form.Control id="objectives" type="text" placeholder="Objectives" value={this.state.objectives} onChange={this.handleObjectiveChange} readOnly />
                </Col>
            </Row>
        </Container>
    }

    randomizeRestrictions = () => {
        var randomWeapons = weapons.getAllowedWeaponTypes(weapons.dualTypeWeapon)
        var randomWeaponMods = weapons.getAllowedWeaponMods()
        var randomNamedWeaponRestriction = weapons.getNamedWeaponRestriction()
        var randomExoticWeaponRestriction = weapons.getExoticWeaponRestrictions()
        var randomSingatureWeaponRestriction = weapons.getSignatureWeaponRestrictions()
        var randomSpecGrenadesRestriction = weapons.getSpecGrenadesRestrictions()
        console.log(`/////////////////////////`)
        console.log(`// Weapon restrictions //`)
        console.log(`/////////////////////////`)
        console.log(`Allowed weapon types: ${randomWeapons}`)
        if (randomWeaponMods.length == 0) {
            console.log(`No weapon mods allowed`)
        } else {
            console.log(`Allowed weapon mods: ${randomWeaponMods}`)
        }
        if (randomNamedWeaponRestriction != null) {
            console.log(`Named weapon restrictions: ${randomNamedWeaponRestriction.id}`)
        }
        if (randomExoticWeaponRestriction != null) {
            console.log(`Exotic weapon restrictions: ${randomExoticWeaponRestriction.id}`)
        }
        if (randomSingatureWeaponRestriction != null) {
            console.log(`Signature weapon restrictions: ${randomSingatureWeaponRestriction.id}`)
        }
        if (randomSpecGrenadesRestriction != null) {
            console.log(`Specialization grenades restrictions: ${randomSpecGrenadesRestriction.id}`)
        }
        var randomNonAvailableGear = gear.getNonAvailableGear(gear.softRestriction)
        var randomGearAttributeRestriction = gear.getGearAttributeRestriction()
        var randomGearAttribueValue = gear.getGearAttributeValue()
        var randomGearModRestriction = gear.getGearModRestriction()
        var randomGearSetRestriction = gear.getGearSetRestriction()
        var randomNamedGearRestriction = gear.getNamedGearRestrictions()
        var randomExoticGearRestriction = gear.getExoticGearRestrictions()
        console.log(`///////////////////////`)
        console.log(`// Gear restrictions //`)
        console.log(`///////////////////////`)
        console.log(`Banned gear: `)
        console.log(randomNonAvailableGear)
        console.log(`Gear attribute restriction: ${randomGearAttributeRestriction.id}, ${randomGearAttribueValue}`)
        console.log(`Gear mod restriction: ${randomGearModRestriction.id}`)
        console.log(`Gear set restriction: ${randomGearSetRestriction.id}`)
        console.log(`Named gear restriction: ${randomNamedGearRestriction.id}`)
        console.log(`Exotic gear restriction: ${randomExoticGearRestriction.id}`)
        return this.setState(state => ({
            allowedWeapons: randomWeapons,
            allowedWeaponMods: randomWeaponMods,
            namedWeapons: randomNamedWeaponRestriction,
            exoticWeapons: randomExoticWeaponRestriction,
            signatureWeapons: randomSingatureWeaponRestriction,
            specGreandes: randomSpecGrenadesRestriction,
            availableGear: randomNonAvailableGear,
            gearAttributeRestriction: randomGearAttributeRestriction,
            gearAttributeValue: randomGearAttribueValue,
            gearModRestriction: randomGearModRestriction,
            gearSetRestriction: randomGearSetRestriction,
            namedGearRestriction: randomNamedWeaponRestriction,
            exoticGearRestriction: randomExoticWeaponRestriction,
            restrictions: "
            ",
            objectives: ""
        }));
    }
}

export default App;