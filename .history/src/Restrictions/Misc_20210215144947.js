import * as utils from '../Utils/Utils.js';
import { Restriction } from '../Restrictions/Restriction.js';

// -- Crafting --

export const noWeaponCrafting = new Restriction(4, 2, "no-weapon-crafting-allowed")
export const noGearCrafting = new Restriction(4, 2, "no-gear-crafting-allowed")
export const noCrafting = new Restriction(4, 2, "no-crafting-allowed")

// -- Vendor --

export const noWeaponBuy = new Restriction(4, 2, "no-weapon-buy-allowed")
export const noGearBuy = new Restriction(4, 2, "no-gear-buy-allowed")
export const noBuy = new Restriction(4, 2, "no-gear-buy-allowed")

// -- Recalibration --

export const noWeaponRecalibration = new Restriction(4, 2, "no-recalibration-allowed")
export const noGearRecalibration = new Restriction(4, 2, "no-recalibration-allowed")
export const noRecalibration = new Restriction(4, 2, "no-recalibration-allowed")

// -- Optimization --

export const noWeaponOptimization = new Restriction(4, 2, "no-optimization-allowed")
export const noGearOptimization = new Restriction(4, 2, "no-optimization-allowed")
export const noOptimization = new Restriction(4, 2, "no-optimization-allowed")

// -- Character --

export const noPerks = new Restriction(4, 2, "no-perks-allowed")
export const noBackup = new Restriction(4, 2, "no-backup-allowed")
export const noStash = new Restriction(4, 2, "no-stash-allowed")

