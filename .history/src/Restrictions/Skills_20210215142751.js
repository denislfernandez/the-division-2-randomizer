import * as utils from '../Utils/Utils.js';
import {Restriction} from '../Restrictions/Restriction.js';

// -- Skills --

export const singleTypeWeapon = new Restriction(4, 2, "single-type-weapon")


// -- Skill mods Sets --