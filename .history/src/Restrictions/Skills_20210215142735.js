import * as utils from '../Utils/Utils.js';
import {Restriction} from '../Restrictions/Restriction.js';

// -- Skills --

// -- Skill mods Sets --

export const softRestriction = new Restriction(3, 2, "soft-gear-restriction")
export const hardRestriction = new Restriction(5, 1, "hard-gear-restriction")

const gearPieces = {
    TACTICAL_511: "5.11 Tactical",
    ARIALDI: "Arialdi Holdings",
    ALPS: "Alps Summit Armaments",
    BADGET: "Badget Tuff",
    BELSTONE: "Belstone Armory",
    CESKA: "Česká Výroba s.r.o.",
    CHINA: "China Light Industries Corporation",
    DOUGLAS: "Douglas & Harding",
    EMPRESS: "Empress International",
    FENRIS: "Fenris Group AB",
    GILA: "Gila Guard",
    GOLAN: "Golan Gear Ltd",
    SOMBRA: "Grupo Sombra S.A.",
    HANA: "Hana-U Corporation",
    MURAKAMI: "Murakami Industries",
    OVERLORD: "Overlord Armaments",
    PETROV: "Petrov Defense Group",
    PROVIDENCE: "Providence Defense",
    RICHTER: "Richter & Kaiser Gmbh",
    SOKOLOV: "Sokolov Concern",
    WALKER: "Walker, Harris & Co",
    WYVERN: "Wyvern Wear",
    YAAHL: "Yaahl Gear",
}

const availableGear = [
    gearPieces.TACTICAL_511,
    gearPieces.ARIALDI,
    gearPieces.ALPS,
    gearPieces.BADGET,
    gearPieces.BELSTONE,
    gearPieces.CESKA,
    gearPieces.CHINA,
    gearPieces.DOUGLAS,
    gearPieces.EMPRESS,
    gearPieces.FENRIS,
    gearPieces.GILA,
    gearPieces.GOLAN,
    gearPieces.SOMBRA,
    gearPieces.HANA,
    gearPieces.MURAKAMI,
    gearPieces.OVERLORD,
    gearPieces.PETROV,
    gearPieces.PROVIDENCE,
    gearPieces.SOKOLOV,
    gearPieces.WALKER,
    gearPieces.WYVERN,
    gearPieces.YAAHL
]

export const gearRestrictions = [softRestriction, hardRestriction]

export function getAvailableGearRestriction() {
    return utils.getRandomValue([...gearRestrictions])
}

export function getAvailableGear(gearRestriction) {
    var restrictionSize = 1
    switch (gearRestriction.id) {
        case softRestriction.id:
            restrictionSize = 16
            break;
        case hardRestriction.id:
            restrictionSize = 11
            break;
    }
    return utils.getRandomValues([...availableGear], restrictionSize)
}

export function getNonAvailableGear(gearRestriction) {
    var restrictionSize = 1
    switch (gearRestriction.id) {
        case softRestriction.id:
            restrictionSize = 6
            break;
        case hardRestriction.id:
            restrictionSize = 11
            break;
    }
    return utils.getRandomValues([...availableGear], restrictionSize)
}


// -- Gear Mods --

const noOffensiveGearMods = new Restriction(4, 2, "no-offensive-gear-mods")
const noDefensiveGearMods = new Restriction(4, 2, "no-defensive-gear-mods")
const noUtilityGearMods = new Restriction(4, 2, "no-utility-gear-mods")
const onlyOffensiveGearMods = new Restriction(4, 2, "only-offensive-gear-mods")
const onlyDeffensiveGearMods = new Restriction(4, 2, "only-defensive-gear-mods")
const onlyUtilityGearMods = new Restriction(4, 2, "only-utility-gear-mods")
const noGearMods = new Restriction(4, 2, "no-gear-mods")

export const gearModsRestrictions = [noOffensiveGearMods, noDefensiveGearMods, noUtilityGearMods, onlyOffensiveGearMods, onlyDeffensiveGearMods, onlyUtilityGearMods, noGearMods]

export function getGearModRestriction() {
    return utils.getRandomValue([...gearModsRestrictions])
}

// -- Gear Sets --

const noGearSets = new Restriction(4, 2, "no-gear-sets")

export function getGearSetRestriction() {
    return noGearSets
}

// -- Named --

const noNamedGear = new Restriction(4, 2, "no-named-gear")
const singleNamedGearPiece = new Restriction(4, 2, "single-named-gear-piece")

export const namedGearRestrictions = [noNamedGear, singleNamedGearPiece]

export function getNamedGearRestrictions() {
    return utils.getRandomValue([...namedGearRestrictions])
}

// -- Exotic --

const noExoticGear = new Restriction(4, 2, "no-exotic-gear")
const singleExoticGearPiece = new Restriction(4, 2, "single-exotic-gear-piece")

export const exoticGearRestrictions = [noExoticGear, singleExoticGearPiece]

export function getExoticGearRestrictions() {
    return utils.getRandomValue([...exoticGearRestrictions])
}

