import * as utils from '../Utils/Utils.js';
import { Restriction } from '../Restrictions/Restriction.js';

// -- Crafting --

export const noWeaponCrafting = new Restriction(4, 2, "no-skills-allowed")

// -- Vendor --
// -- Recalibration --
// -- Optimization --
// -- Character --
