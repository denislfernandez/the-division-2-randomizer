import * as utils from '../Utils/Utils.js';
import {Restriction} from '../Restrictions/Restriction.js';

// -- Skills --

export const noSkillsAllowed = new Restriction(4, 2, "no-skills-allowed")
export const singleSkillAllowed = new Restriction(4, 2, "no-skills-allowed")
export const onlyHealingSkillsAllowed = new Restriction(4, 2, "no-skills-allowed")
export const singleHealingSkillAllowed = new Restriction(4, 2, "no-skills-allowed")
export const singleHealingSkillAllowed = new Restriction(4, 2, "no-skills-allowed")
export const noSkillsAllowed = new Restriction(4, 2, "no-skills-allowed")

const skillsRestrictions = [
    noSkillModsAllowed, singleSkillAllowed, onlyHealingSkillsAllowed, singleHealingSkillAllowed, noSkillsAllowed
]

const skill = {
    PULSE: "Pulse",
    TURRET: "Turret",
    SEEKER_MINE: "Seeker Mine",
    SHIELD: "Ballistic Shield",
    LAUNCHER: "Chem Launcher",
    DRONE: "Drone",
    FIREFLY: "Firefly",
    HIVE: "Hive"
}

const wonySkill = {
    STICKY: "Sticky Bomb",
    TRAP: "Trap",
    DECOY: "Decoy"
}

const baseSkills = [
    skill.PULSE, skill.TURRENT, skill.SEEKER_MINE, skill.SHIELD, skill.LAUNCHER, skill.DRONE, skill.FIREFLY, skill.HIVE
]

const allSkills = [
    ...baseSkills, wonySkill.STICKY, wonySkill.TRAP, wonySkill.DECOY
]

export function getSkillsRestriction() {
    return utils.getRandomValue([...typeRestrictions])
}

// -- Skill mods Sets --

export const noSkillModsAllowed = new Restriction(4, 2, "no-skill-mods-allowed")