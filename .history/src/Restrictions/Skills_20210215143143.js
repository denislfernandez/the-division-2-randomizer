import * as utils from '../Utils/Utils.js';
import {Restriction} from '../Restrictions/Restriction.js';

// -- Skills --

export const noSkillsAllowed = new Restriction(4, 2, "no-skills-allowed")
export const singleSkillAllowed = new Restriction(4, 2, "no-skills-allowed")
export const onlyHealingSkillsAllowed = new Restriction(4, 2, "no-skills-allowed")
export const singleHealingSkillAllowed = new Restriction(4, 2, "no-skills-allowed")
export const noSkillsAllowed = new Restriction(4, 2, "no-skills-allowed")

const skills = {
    PULSE: "Pulse",
    TURRENT: "Turret",
    SEEKER_MINE: "Seeker Mine",
    SHIELD: "Ballistic Shield",
    LAUNCHER: "Rifle",
    DRONE: "Shotgun"
    FIREFLY: "Shotgun"
    HIVE: "Shotgun"
}


// -- Skill mods Sets --

export const noSkillModsAllowed = new Restriction(4, 2, "no-skill-mods-allowed")