import * as utils from '../Utils/Utils.js';
import { Restriction } from '../Restrictions/Restriction.js';

// -- Crafting --

export const noWeaponCrafting = new Restriction(4, 2, "no-weapon-crafting-allowed")
export const noGearCrafting = new Restriction(4, 2, "no-gear-crafting-allowed")

// -- Vendor --

export const noWeaponBuy = new Restriction(4, 2, "no-weapon-crafting-allowed")
export const noGearCrafting = new Restriction(4, 2, "no-gear-crafting-allowed")

// -- Recalibration --
// -- Optimization --
// -- Character --
