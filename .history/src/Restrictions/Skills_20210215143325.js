import * as utils from '../Utils/Utils.js';
import {Restriction} from '../Restrictions/Restriction.js';

// -- Skills --

export const noSkillsAllowed = new Restriction(4, 2, "no-skills-allowed")
export const singleSkillAllowed = new Restriction(4, 2, "no-skills-allowed")
export const onlyHealingSkillsAllowed = new Restriction(4, 2, "no-skills-allowed")
export const singleHealingSkillAllowed = new Restriction(4, 2, "no-skills-allowed")
export const noSkillsAllowed = new Restriction(4, 2, "no-skills-allowed")

const skills = {
    PULSE: "Pulse",
    TURRET: "Turret",
    SEEKER_MINE: "Seeker Mine",
    SHIELD: "Ballistic Shield",
    LAUNCHER: "Chem Launcher",
    DRONE: "Drone",
    FIREFLY: "Firefly",
    HIVE: "Hive"
}

const wonySkills = {
    STICKY: "Sticky Bomb",
    TRAP: "Trap",
    DECOY: "Decoy"
}

const baseSkills = [
    skills.PULSE, skills.TURRENT, skill.SEEKER_MINE, skills.SHIELD, skills.LAUNCHER, skills.DRONE, skills.FIREFLY, skills.HIVE
]

// -- Skill mods Sets --

export const noSkillModsAllowed = new Restriction(4, 2, "no-skill-mods-allowed")