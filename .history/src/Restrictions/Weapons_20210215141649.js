import * as utils from '../Utils/Utils.js';

// -- Types --

export const singleTypeWeapon = new utils.Restriction(4, 2, "single-type-weapon")
export const dualTypeWeapon = new utils.Restriction(3, 3, "dual-type-weapon")

const weaponTypes = {
    AR: "AR",
    SMG: "SMG",
    LMG: "LMG",
    MARKSMAN: "Marksman",
    RIFLE: "Rifle",
    SHOTGUN: "Shotgun"
}

const availableWeaponTypes = [
    weaponTypes.AR, weaponTypes.SMG, weaponTypes.LMG, weaponTypes.MARKSMAN, weaponTypes.RIFLE, weaponTypes.SHOTGUN
]

export const typeRestrictions = [singleTypeWeapon, dualTypeWeapon]

export function getWeaponTypeRestriction() {
    return utils.getRandomValue([...typeRestrictions])
}

export function getAllowedWeaponTypes(weaponTypeRestriction) {
    var restrictionSize = 1
    switch (weaponTypeRestriction.id) {
        case singleTypeWeapon.id:
            restrictionSize = 1
            break;
        case dualTypeWeapon.id:
            restrictionSize = 2
            break;
    }
    return utils.getRandomValues([...availableWeaponTypes], restrictionSize)
}

// -- Mods --

const singleModType = new utils.Restriction(3, 2, "single-mod-type")
const noModsAllowed = new utils.Restriction(5, 1, "no-mods-allowed")

const modTypes = {
    OPTICS: "Optics",
    MAGAZINE: "Magazine",
    UNDERBARREL: "Underbarrel",
    MUZZLE: "Muzzle"
}

const availableModTypes = [
    modTypes.OPTICS, modTypes.MAGAZINE, modTypes.UNDERBARREL, modTypes.MUZZLE
]

export const modRestrictions = [singleModType, noModsAllowed]

export function getAllowedWeaponMods() {
    var restriction = utils.getRandomValue([...modRestrictions])
    var restrictionSize = 1
    switch (restriction.id) {
        case singleModType.id:
            restrictionSize = 1
            break;
        case noModsAllowed.id:
            restrictionSize = 0
            break;
    }
    return utils.getRandomValues([...availableModTypes], restrictionSize)
}

// -- Named --

const singleNamedWeapon = new utils.Restriction(4, 2, "single-named-weapon")
const singleNamedWeaponWithPistol = new utils.Restriction(4, 2, "single-named-weapon-pistol")
const noNamedWeapons = new utils.Restriction(4, 2, "no-named-weapon")

export const namedWeaponRestrictions = [singleNamedWeapon, singleNamedWeaponWithPistol, noNamedWeapons]

export function getNamedWeaponRestriction() {
    return utils.getRandomValue([...namedWeaponRestrictions])
}

// -- Exotics --

const singleExoticWeapon = new utils.Restriction(4, 2, "single-exotic-weapon")
const singleExoticWeaponWithPistol = new utils.Restriction(4, 2, "single-exotic-weapon-pistol")
const noExoticWeapons = new utils.Restriction(4, 2, "no-exotic-weapon")

export const exoticWeaponRestrictions = [singleExoticWeapon, singleExoticWeaponWithPistol, noExoticWeapons]

export function getExoticWeaponRestrictions() {
    return utils.getRandomValue([...exoticWeaponRestrictions])
}

// -- Signature --

const noSignatureWeapons = new utils.Restriction(4, 2, "no-signature-weapon")

export function getSignatureWeaponRestrictions() {
    return utils.getRandomValue([noSignatureWeapons])
}

// -- Grenades --

const noSpecGrenades = new utils.Restriction(4, 2, "no-spec-grenades")

export function getSpecGrenadesRestrictions() {
    return utils.getRandomValue([noSpecGrenades])
}


