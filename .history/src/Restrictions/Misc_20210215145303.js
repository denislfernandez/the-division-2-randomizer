import * as utils from '../Utils/Utils.js';
import { Restriction } from '../Restrictions/Restriction.js';

// -- Crafting --

export const noWeaponCrafting = new Restriction(4, 2, "no-weapon-crafting-allowed")
export const noGearCrafting = new Restriction(4, 2, "no-gear-crafting-allowed")
export const noCrafting = new Restriction(4, 2, "no-crafting-allowed")

const craftingRestrictions = [
    noWeaponCrafting, noGearCrafting, noCrafting
]

export function getCraftingRestrictions() {
    return utils.getRandomValue([...craftingRestrictions])
}

// -- Vendor --

export const noWeaponBuy = new Restriction(4, 2, "no-weapon-buy-allowed")
export const noGearBuy = new Restriction(4, 2, "no-gear-buy-allowed")
export const noBuy = new Restriction(4, 2, "no-buy-allowed")

const buyRestrictions = [
    noWeaponBuy, noGearBuy, noBuy
]

export function getBuyRestrictions() {
    return utils.getRandomValue([...buyRestrictions])
}

// -- Recalibration --

export const noWeaponRecalibration = new Restriction(4, 2, "no-weapon-recalibration-allowed")
export const noGearRecalibration = new Restriction(4, 2, "no-gear-recalibration-allowed")
export const noRecalibration = new Restriction(4, 2, "no-recalibration-allowed")

const recalibrationRestrictions = [
    noWeaponRecalibration, noGearRecalibration, noRecalibration
]

export function getRecalibrationRestrictions() {
    return utils.getRandomValue([...recalibrationRestrictions])
}

// -- Optimization --

export const noWeaponOptimization = new Restriction(4, 2, "no-weapon-optimization-allowed")
export const noGearOptimization = new Restriction(4, 2, "no-gear-optimization-allowed")
export const noOptimization = new Restriction(4, 2, "no-optimization-allowed")

const optimizationRestrictions = [
    noWeaponOptimization, noGearOptimization, noOptimization
]

export function getOptimizationRestrictions() {
    return utils.getRandomValue([...optimizationRestrictions])
}

// -- Character --

export const noPerks = new Restriction(4, 2, "no-perks-allowed")
export const noBackup = new Restriction(4, 2, "no-backup-allowed")
export const noStash = new Restriction(4, 2, "no-stash-allowed")

const characterRestrictions = [
    noPerks, noBackup, noStash
]

export function getCharacterRestrictions() {
    var restrictionSize = utils.getRandomValue([1, 2, 3])
    return utils.getRandomValue([...characterRestrictions])
}