class Restriction {
    constructor(difficulty, speed, id) {
        this.difficulty = difficulty;
      this.speed = speed;
      this.id = id;
    }
  }

function getRestrictions(array, numberOfValues) {
    restrictions = []
    maxValues = Math.max(numberOfValues, array.length)
    processedArray = array
    for (var i = 0; i < maxValues; i++) {
        index = Math.floor(Math.random() * processedArray.length)
        restriction = processedArray[index]
        restrictions.push(restriction)
        processedArray = processedArray.splice(index, 1)
    }
    return restrictions;
}
