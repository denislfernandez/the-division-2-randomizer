import './App.css';
import React from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Alert from 'react-bootstrap/Alert'
import 'bootstrap/dist/css/bootstrap.min.css';
import * as utils from './Utils.js';
import * as weapons from './Weapons.js';
import * as gear from './Gear.js';
// import i18n from "i18next";
// import { useTranslation, initReactI18next } from "react-i18next";

// i18n
//     .use(initReactI18next) // passes i18n down to react-i18next
//     .init({
//         resources: {
//             en: {
//                 translation: {
//                     "Welcome to React": "Welcome to React and react-i18next"
//                 }
//             }
//         },
//         lng: "en",
//         fallbackLng: "en",

//         interpolation: {
//             escapeValue: false
//         }
//     });
class App extends React.Component {
    render() {
        randomizeRestrictions()
        return <Container>
            <Row>
                <Col>
                    <Alert variant="success">
                        <Alert.Heading>Welcome to The Division 2 Randomizer</Alert.Heading>
                        <p>
                            Aww yeah, you successfully read this important alert message. This example
                            text is going to run a bit longer so that you can see how spacing within an
                            alert works with this kind of content.
                        </p>
                        <hr />
                        <p className="mb-0">
                            Whenever you need to, be sure to use margin utilities to keep things nice
                            and tidy.
                        </p>
                    </Alert>
                    <Button variant="primary" size="lg" block>
                        Randomize
                    </Button>
                    <Form.Control type="text" placeholder="Restrictions" readOnly />
                    <Form.Control type="text" placeholder="Objectives" readOnly />
                </Col>
            </Row>
        </Container>
    }
}

export default App;

function randomizeRestrictions() {
    var allowedWeapons = weapons.getAllowedWeaponTypes(weapons.dualTypeWeapon)
    var allowedWeaponMods = weapons.getAllowedWeaponMods()
    var namedWeapons = weapons.getNamedWeaponRestriction()
    var exoticWeapons = weapons.getExoticWeaponRestrictions()
    var signatureWeapons = weapons.getSignatureWeaponRestrictions()
    var specGreandes = weapons.getSpecGrenadesRestrictions()
    console.log(`/////////////////////////`)
    console.log(`// Weapon restrictions //`)
    console.log(`/////////////////////////`)
    console.log(`Allowed weapon types: ${allowedWeapons}`)
    if (allowedWeaponMods.length == 0) {
        console.log(`No weapon mods allowed`)
    } else {
        console.log(`Allowed weapon mods: ${allowedWeaponMods}`)
    }
    if (namedWeapons != null) {
        console.log(`Named weapon restrictions: ${namedWeapons.id}`)
    }
    if (exoticWeapons != null) {
        console.log(`Exotic weapon restrictions: ${exoticWeapons.id}`)
    }
    if (signatureWeapons != null) {
        console.log(`Signature weapon restrictions: ${signatureWeapons.id}`)
    }
    if (specGreandes != null) {
        console.log(`Specialization grenades restrictions: ${specGreandes.id}`)
    }
    var availableGear = gear.getNonAvailableGear(gear.softRestriction)
    var gearAttributeRestriction = gear.getGearAttributeRestriction()
    var gearAttributeValue = gear.getGearAttributeValue()
    var gearModRestriction = gear.getGearModRestriction()
    var gearSetRestriction = gear.getGearSetRestriction()
    var namedGearRestriction = gear.getNamedGearRestrictions()
    var exoticGearRestriction = gear.getExoticGearRestrictions()
    console.log(`///////////////////////`)
    console.log(`// Gear restrictions //`)
    console.log(`///////////////////////`)
    console.log(`Banned gear: `)
    console.log(availableGear)
    console.log(`Gear attribute restriction: ${gearAttributeRestriction.id}, ${gearAttributeValue}`)
    console.log(`Gear mod restriction: ${gearModRestriction.id}`)
    console.log(`Gear set restriction: ${gearSetRestriction.id}`)
    console.log(`Named gear restriction: ${namedGearRestriction.id}`)
    console.log(`Exotic gear restriction: ${exoticGearRestriction.id}`)
}