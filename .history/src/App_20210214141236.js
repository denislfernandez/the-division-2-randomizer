import './App.css';
import FormTest from './Form.js';
import * as weapons from './Weapons.js';

class App extends React.Component {
    render() {
        randomizeRestrictions()
        return FormTest()
    }
}

export default App;

function randomizeRestrictions() {
    var weaponType = weapons.getRandomWeaponTypes()
    console.log(weaponType)
}