import * as utils from './Utils.js';

// -- Types --

const singleGearTypeAllowed = new utils.Restriction(4, 2, "single-gear-type")
const noGearTypeAllowed = new utils.Restriction(3, 3, "no-gear-type")

const gearTypes = {
    OFFENSIVE: "ofensive",
    DEFENSIVE: "deffensive",
    UTILITY: "utility"
}

const availableGearTypes = [
    gearTypes.OFFENSIVE, gearTypes.DEFENSIVE, gearTypes.UTILITY
]

export const typeRestrictions = [singleGearTypeAllowed, noGearTypeAllowed]

export function getGearValues() {
    return utils.getRandomValues([...availableGearTypes], 1)
}

export function getGearRestriction(enabledRestrictions) {
    if (enabledRestrictions) {
        var restriction = utils.getRandomValue([...typeRestrictions])
        return restriction
    } else {
        return null
    }
}

// -- Mods --

const softRestriction = new utils.Restriction(3, 2, "soft-gear-restriction")
const hardRestriction = new utils.Restriction(5, 1, "hard-gear-restriction")

const gear = {
    TACTICAL_511: "5.11 Tactical",
    ARIALDI: "Arialdi Holdings",
    ALPS: "Alps Summit Armaments",
    BADGET: "Badget Tuff",
    BELSTONE: "Belstone Armory",
    CESKA: "muzzle",
    CHINA: "China Light Industries Corporation",
    DOUGLAS: "Douglas & Harding",
    EMPRESS: "muzzle",
    FENRIS: "muzzle",
    GILA: "muzzle",
    GOLAN: "muzzle",
    SOMBRA: "muzzle",
    HANA: "muzzle",
    MURAKAMI: "muzzle",
    OVERLORD: "muzzle",
    PETROV: "muzzle",
    PROVIDENCE: "muzzle",
    RICHTER: "muzzle",
    SOKOLOV: "muzzle",
    WALKER: "muzzle",
    WYVERN: "muzzle",
    YAAHL: "muzzle",
}

const availableModTypes = [
    modTypes.OPTICS, modTypes.MAGAZINE, modTypes.UNDERBARREL, modTypes.MUZZLE
]

export const modRestrictions = [singleModType, noModsAllowed]

export function getAllowedWeaponMods(enabledRestrictions) {
    if (enabledRestrictions) {
        var restriction = utils.getRandomValue([...modRestrictions])
        var restrictionSize = 1
        switch (restriction.id) {
            case singleModType.id:
                restrictionSize = 1
                break;
            case noModsAllowed.id:
                restrictionSize = 0
                break;
        }
        return utils.getRandomValues([...availableModTypes], restrictionSize)
    } else {
        return [...availableModTypes]
    }
}

// -- Named --

const singleNamedWeapon = new utils.Restriction(4, 2, "single-named-weapon")
const singleNamedWeaponWithPistol = new utils.Restriction(4, 2, "single-named-weapon-pistol")
const noNamedWeapons = new utils.Restriction(4, 2, "no-named-weapon")

export const namedWeaponRestrictions = [singleNamedWeapon, singleNamedWeaponWithPistol, noNamedWeapons]

export function getNamedWeaponRestriction(enabledRestrictions) {
    if (enabledRestrictions) {
        return utils.getRandomValue([...namedWeaponRestrictions])
    } else {
        return null
    }
}

// -- Exotics --

const singleExoticWeapon = new utils.Restriction(4, 2, "single-exotic-weapon")
const singleExoticWeaponWithPistol = new utils.Restriction(4, 2, "single-exotic-weapon-pistol")
const noExoticWeapons = new utils.Restriction(4, 2, "no-exotic-weapon")

export const exoticWeaponRestrictions = [singleExoticWeapon, singleExoticWeaponWithPistol, noExoticWeapons]

export function getExoticWeaponRestrictions(enabledRestrictions) {
    if (enabledRestrictions) {
        return utils.getRandomValue([...exoticWeaponRestrictions])
    } else {
        return null
    }
}

// -- Signature --

const noSignatureWeapons = new utils.Restriction(4, 2, "no-signature-weapon")

export function getSignatureWeaponRestrictions(enabledRestrictions) {
    if (enabledRestrictions) {
        return utils.getRandomValue([noSignatureWeapons])
    } else {
        return null
    }
}

// -- Grenades --

const noSpecGrenades = new utils.Restriction(4, 2, "no-spec-grenades")

export function getSpecGrenadesRestrictions(enabledRestrictions) {
    if (enabledRestrictions) {
        return utils.getRandomValue([noSpecGrenades])
    } else {
        return null
    }
}


