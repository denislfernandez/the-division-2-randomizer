import * as utils from './Utils.js';

const singleModType = new utils.Restriction(3, 2, "single-mod-type")
const noModsAllowed = new utils.Restriction(5, 1, "no-mods-allowed")

const modTypes = {
    OPTICS: "optics",
    MAGAZINE: "magazine",
    UNDERBARREL: "underbarrel",
    MUZZLE: "muzzle"
}

const availableModTypes = [
    modTypes.OPTICS, modTypes.MAGAZINE, modTypes.UNDERBARREL, modTypes.MUZZLE
]

export const modRestrictions = [singleModType, noModsAllowed]

export function getAllowedMods(enabledRestrictions) {
    if (enabledRestrictions) {
        var modRestriction = utils.getRandomValue([...modRestrictions])
        var restrictionSize = 1
        switch (modRestriction.id) {
            case singleModType.id:
                restrictionSize = 1
                break;
            case noModsAllowed.id:
                restrictionSize = 0
                break;
        }
        console.log(modRestriction)
        return utils.getRandomValues([...availableWeaponTypes], restrictionSize)
    } else {
        return [...availableModTypes]
    }
}

// const noModsAllowed = new Restriction(4, 1, "no-mods-allowed")
// const singleModTypeAllowed = new Restriction(3, 2, "single-mod-type-allowed")

// export const modRestrictions = [noModsAllowed, singleModTypeAllowed]