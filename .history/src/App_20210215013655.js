import './App.css';
import React from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Card from 'react-bootstrap/Card'
import 'bootstrap/dist/css/bootstrap.min.css';
import * as utils from './Utils.js';
import * as weapons from './Weapons.js';
import * as gear from './Gear.js';
class App extends React.Component {
    render() {
        randomizeRestrictions()
        return <Container>
            <Row>
                <Col>
                    <Form>
                        <Card border="primary" style={{ width: '18rem' }}>
                            <Card.Header>Weapon types</Card.Header>
                            <Card.Body>
                                <Card.Title>Primary Card Title</Card.Title>
                                <Card.Text>
                                    Some quick example text to build on the card title and make up the bulk
                                    of the card's content.
                                </Card.Text>
                            </Card.Body>
                        </Card>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" />
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                    </Form.Text>
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" />
                        </Form.Group>
                        <Form.Group controlId="formBasicCheckbox">
                            <Form.Check type="checkbox" label="Check me out" />
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Submit
        </Button>
                    </Form></Col>
            </Row>
        </Container>
        return
    }
}

class FormGroup extends React.Component {
    render() {
        return 
    }
}

export default App;

function randomizeRestrictions() {
    var allowedWeapons = weapons.getAllowedWeaponTypes(true)
    var allowedWeaponMods = weapons.getAllowedWeaponMods(true)
    var namedWeapons = weapons.getNamedWeaponRestriction(utils.enabledRestriction())
    var exoticWeapons = weapons.getExoticWeaponRestrictions(utils.enabledRestriction())
    var signatureWeapons = weapons.getSignatureWeaponRestrictions(utils.enabledRestriction())
    var specGreandes = weapons.getSpecGrenadesRestrictions(utils.enabledRestriction())
    console.log(`Allowed weapon types: ${allowedWeapons}`)
    if (allowedWeaponMods.length == 0) {
        console.log(`No weapon mods allowed`)
    } else {
        console.log(`Allowed weapon mods: ${allowedWeaponMods}`)
    }
    if (namedWeapons != null) {
        console.log(`Named weapon restrictions: ${namedWeapons.id}`)
    }
    if (exoticWeapons != null) {
        console.log(`Exotic weapon restrictions: ${exoticWeapons.id}`)
    }
    if (signatureWeapons != null) {
        console.log(`Signature weapon restrictions: ${signatureWeapons.id}`)
    }
    if (specGreandes != null) {
        console.log(`Specialization grenades restrictions: ${specGreandes.id}`)
    }
    var gear = gear.getAvailableGear(gear.softRestriction)
    var gearRestriction = gear.getGearRestriction()
}