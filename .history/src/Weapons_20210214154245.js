import * as utils from './Utils.js';

// -- Types --

const singleTypeWeapon = new utils.Restriction(4, 2, "single-type-weapon")
const dualTypeWeapon = new utils.Restriction(3, 3, "dual-type-weapon")

const weaponTypes = {
    AR: "ar",
    SMG: "smg",
    LMG: "lmg",
    MARKSMAN: "marksman",
    RIFLE: "rifle",
    SHOTGUN: "shotgun"
}

const availableWeaponTypes = [
    weaponTypes.AR, weaponTypes.SMG, weaponTypes.LMG, weaponTypes.MARKSMAN, weaponTypes.RIFLE, weaponTypes.SHOTGUN
]

export const typeRestrictions = [singleTypeWeapon, dualTypeWeapon]

export function getAllowedWeaponTypes(enabledRestrictions) {
    if (enabledRestrictions) {
        var restriction = utils.getRandomValue([...typeRestrictions])
        var restrictionSize = 1
        switch (restriction.id) {
            case singleTypeWeapon.id:
                restrictionSize = 1
                break;
            case dualTypeWeapon.id:
                restrictionSize = 2
                break;
        }
        return utils.getRandomValues([...availableWeaponTypes], restrictionSize)
    } else {
        return [...availableWeaponTypes]
    }
}

// -- Mods --

const singleModType = new utils.Restriction(3, 2, "single-mod-type")
const noModsAllowed = new utils.Restriction(5, 1, "no-mods-allowed")

const modTypes = {
    OPTICS: "optics",
    MAGAZINE: "magazine",
    UNDERBARREL: "underbarrel",
    MUZZLE: "muzzle"
}

const availableModTypes = [
    modTypes.OPTICS, modTypes.MAGAZINE, modTypes.UNDERBARREL, modTypes.MUZZLE
]

export const modRestrictions = [singleModType, noModsAllowed]

export function getAllowedWeaponMods(enabledRestrictions) {
    if (enabledRestrictions) {
        var restriction = utils.getRandomValue([...modRestrictions])
        var restrictionSize = 1
        switch (restriction.id) {
            case singleModType.id:
                restrictionSize = 1
                break;
            case noModsAllowed.id:
                restrictionSize = 0
                break;
        }
        return utils.getRandomValues([...availableModTypes], restrictionSize)
    } else {
        return [...availableModTypes]
    }
}

// -- Named Weapons --

const singleNamedWeapon = new utils.Restriction(4, 2, "single-type-weapon")
const singleNamedWeaponWithPistol = new utils.Restriction(4, 2, "single-type-weapon")
const noNamedWeapons = new utils.Restriction(4, 2, "single-type-weapon")

// -- Exotics Weapons --

const singleExoticWeapon = new utils.Restriction(4, 2, "single-type-weapon")
const singleExoticWeaponWithPistol = new utils.Restriction(4, 2, "single-type-weapon")
const noExoticWeapons = new utils.Restriction(4, 2, "single-type-weapon")

const noSignatureWeapons = new utils.Restriction(4, 2, "single-type-weapon")

const noSpecGrenades = new utils.Restriction(4, 2, "single-type-weapon")

