import './App.css';
import React from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Alert from 'react-bootstrap/Alert'
import 'bootstrap/dist/css/bootstrap.min.css';
import * as utils from './Utils.js';
import * as weapons from './Weapons.js';
import * as gear from './Gear.js';
// import i18n from "i18next";
// import { useTranslation, initReactI18next } from "react-i18next";

// i18n
//     .use(initReactI18next) // passes i18n down to react-i18next
//     .init({
//         resources: {
//             en: {
//                 translation: {
//                     "Welcome to React": "Welcome to React and react-i18next"
//                 }
//             }
//         },
//         lng: "en",
//         fallbackLng: "en",

//         interpolation: {
//             escapeValue: false
//         }
//     });
class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            restrictions: "",
            restrictionsRows: 1,
            objectives: "",
            objectivesRows: 0,
        };  
    }

    handleRestrictionChange(event) {    
        this.setState({restrictions: event.target.value});  
    }

    handleObjectiveChange(event) {    
        this.setState({objectives: event.target.value});  
    }

    render() {
        return <Container>
            <Row>
                <Col>
                    <Alert variant="success">
                        <Alert.Heading>Welcome to The Division 2 Randomizer!</Alert.Heading>
                        <p>
                            This tool is intended to be used when playing hardcore mode (or normal mode if you want a list of restrictions/objectives to be following)
                            to make each run you make a bit different. 
                        </p>
                        <hr></hr>
                        <p>
                            With the following options you can customize the restriction and objectives randomization.
                            When you have finished setting up the randomization click on the <strong>Randomize</strong> button and the results will be displayed in the final
                            text areas
                        </p>
                    </Alert>
                    <Button onClick={this.randomizeRestrictions} variant="primary" size="lg" block>
                        Randomize
                    </Button>
                    <Form.Control as="textarea" rows={this.state.restrictionsRows} id="restrictions" type="text" placeholder="Restrictions" value={this.state.restrictions} onChange={this.handleRestrictionChange} readOnly />
                    <Form.Control id="objectives" type="text" placeholder="Objectives" value={this.state.objectives} onChange={this.handleObjectiveChange} readOnly />
=
                </Col>
            </Row>
        </Container>
    }

    randomizeRestrictions = () => {
        var restrictions = []
        var resRows = 0
        var gearList = []
        var randomWeapons = weapons.getAllowedWeaponTypes(weapons.dualTypeWeapon)
        restrictions.push(
            `Weapon restrictions`,
            `   Allowed weapon types: ${randomWeapons}`,
        )
        resRows++
        var randomWeaponMods = weapons.getAllowedWeaponMods()
        restrictions.push(
            `   Allowed weapon mods: ${randomWeaponMods}`,
        )
        resRows++
        if (utils.enabledRestriction()) {
            var randomNamedWeaponRestriction = weapons.getNamedWeaponRestriction()
            restrictions.push(
                `   Named weapon restrictions: ${randomNamedWeaponRestriction.id}`,
            )
            resRows++
        }
        if (utils.enabledRestriction()) {
            var randomExoticWeaponRestriction = weapons.getExoticWeaponRestrictions()
            restrictions.push(
                `   Exotic weapon restrictions: ${randomExoticWeaponRestriction.id}`,
            )
            resRows++
        }
        if (utils.enabledRestriction()) {
            var randomSingatureWeaponRestriction = weapons.getSignatureWeaponRestrictions()
            restrictions.push(
                `   Signature weapon restrictions: ${randomSingatureWeaponRestriction.id}`,
            )
            resRows++
        }
        if (utils.enabledRestriction()) {
            var randomSpecGrenadesRestriction = weapons.getSpecGrenadesRestrictions()
            restrictions.push(
                `   Specialization grenades restrictions: ${randomSpecGrenadesRestriction.id}`,
            )
            resRows++
        }
        restrictions.push(``)
        resRows++
        var randomNonAvailableGear = gear.getNonAvailableGear(gear.softRestriction)
        for (var i = 0; i < randomNonAvailableGear.length; i++) {
            gearList.push(`     ${randomNonAvailableGear[i]}`)
        }
        restrictions.push(
            `Gear restrictions`,
        // var randomGearAttributeRestriction = gear.getGearAttributeRestriction()
        // var randomGearAttribueValue = gear.getGearAttributeValue()
        // var randomGearModRestriction = gear.getGearModRestriction()
        // var randomGearSetRestriction = gear.getGearSetRestriction()
        // var randomNamedGearRestriction = gear.getNamedGearRestrictions()
        // var randomExoticGearRestriction = gear.getExoticGearRestrictions()

        // restrictions.push(
        //     `Gear restrictions`,
        //     `   Banned gear: `,
        //     `${gearList.join(`\n`)}`,
        //     `   Gear attribute restriction: ${randomGearAttributeRestriction.id}, ${randomGearAttribueValue}`,
        //     `   Gear mod restriction: ${randomGearModRestriction.id}`,
        //     `   Gear set restriction: ${randomGearSetRestriction.id}`,
        //     `   Named gear restriction: ${randomNamedGearRestriction.id}`,
        //     `   Exotic gear restriction: ${randomExoticGearRestriction.id}`
        // )

        return this.setState(state => ({
            restrictions: restrictions.join("\n"),
            restrictionsRows: resRows,
            objectives: "",
            objectivesRows: 0
        }));
    }
}

export default App;