import * as utils from './Utils.js';

const singleModType = new utils.Restriction(3, 2, "single-mod-type")
const noModsAllowed = new utils.Restriction(5, 1, "no-mods-allowed")

const modTypes = {
    OPTICS: "optics",
    MAGAZINE: "magazine",
    UNDERBARREL: "underbarrel",
    MUZZLE: "muzzle"
}

const availableModTypes = [
    modTypes.OPTICS, modTypes.MAGAZINE, modTypes.UNDERBARREL, modTypes.MUZZLE
]

export const modRestrictions = [singleModType, noModsAllowed]

export function getAllowedWeaponMods(enabledRestrictions) {
    if (enabledRestrictions) {
        var restriction = utils.getRandomValue([...modRestrictions])
        var restrictionSize = 1
        switch (restriction.id) {
            case singleModType.id:
                restrictionSize = 1
                break;
            case noModsAllowed.id:
                restrictionSize = 0
                break;
        }
        return utils.getRandomValues([...availableModTypes], restrictionSize)
    } else {
        return [...availableModTypes]
    }
}