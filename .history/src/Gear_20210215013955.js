import * as utils from './Utils.js';

// -- Gear Attributes --

const singleGearTypeAllowed = new utils.Restriction(4, 2, "single-gear-type")
const noGearTypeAllowed = new utils.Restriction(3, 3, "no-gear-type")

const gearTypes = {
    OFFENSIVE: "ofensive",
    DEFENSIVE: "deffensive",
    UTILITY: "utility"
}

const availableGearTypes = [
    gearTypes.OFFENSIVE, gearTypes.DEFENSIVE, gearTypes.UTILITY
]

export const typeRestrictions = [singleGearTypeAllowed, noGearTypeAllowed]

export function getGearValues() {
    return utils.getRandomValues([...availableGearTypes], 1)
}

export function getGearRestriction(enabledRestrictions) {
    if (enabledRestrictions) {
        var restriction = utils.getRandomValue([...typeRestrictions])
        return restriction
    } else {
        return null
    }
}

// -- Brand Sets --

export const softRestriction = new utils.Restriction(3, 2, "soft-gear-restriction")
export const hardRestriction = new utils.Restriction(5, 1, "hard-gear-restriction")

const gear = {
    TACTICAL_511: "5.11 Tactical",
    ARIALDI: "Arialdi Holdings",
    ALPS: "Alps Summit Armaments",
    BADGET: "Badget Tuff",
    BELSTONE: "Belstone Armory",
    CESKA: "muzzle",
    CHINA: "China Light Industries Corporation",
    DOUGLAS: "Douglas & Harding",
    EMPRESS: "Empress International",
    FENRIS: "Fenris Group AB",
    GILA: "Gila Guard",
    GOLAN: "Golan Gear Ltd",
    SOMBRA: "Grupo Sombra S.A.",
    HANA: "Hana-U Corporation",
    MURAKAMI: "Murakami Industries",
    OVERLORD: "Overlord Armaments",
    PETROV: "Petrov Defense Group",
    PROVIDENCE: "Providence Defense",
    RICHTER: "Richter & Kaiser Gmbh",
    SOKOLOV: "Sokolov Concern",
    WALKER: "Walker, Harris & Co",
    WYVERN: "Wyvern Wear",
    YAAHL: "Yaahl Gear",
}

const availableGear 

export const gearRestrictions = [softRestriction, hardRestriction]

export function getAvailableGear(gearRestriction) {
    var restrictionSize = 1
    switch (gearRestriction.id) {
        case softRestriction.id:
            restrictionSize = 16
            break;
        case hardRestriction.id:
            restrictionSize = 11
            break;
    }
    return utils.getRandomValues([...availableGear], restrictionSize)
}

// -- Gear Mods --

const noOffensiveGearMods = new utils.Restriction(4, 2, "no-offensive-gear-mods")
const noDefensiveGearMods = new utils.Restriction(4, 2, "no-deffensive-gear-mods")
const noUtilityGearMods = new utils.Restriction(4, 2, "no-utility-gear-mods")
const onlyOffensiveGearMods = new utils.Restriction(4, 2, "only-offensive-gear-mods")
const onlyDeffensiveGearMods = new utils.Restriction(4, 2, "only-defensive-gear-mods")
const onlyUtilityGearMods = new utils.Restriction(4, 2, "only-utility-gear-mods")
const noGearMods = new utils.Restriction(4, 2, "no-gear-mods")

export const gearModsRestrictions = [noOffensiveGearMods, noDefensiveGearMods, noUtilityGearMods, onlyOffensiveGearMods, onlyDeffensiveGearMods, onlyUtilityGearMods, noGearMods]

export function getGearModRestriction() {
    return utils.getRandomValue([...gearModsRestrictions])
}

// -- Gear Sets --

const noGearSets = new utils.Restriction(4, 2, "no-gear-sets")

export function getGearSetRestriction() {
    return noGearSets
}

// -- Named --

const noNamedGear = new utils.Restriction(4, 2, "no-named-gear")
const singleNamedGearPiece = new utils.Restriction(4, 2, "single-named-gear-piece")

export const namedGearRestrictions = [noNamedGear, singleNamedGearPiece]

export function getNamedGearRestrictions() {
    return utils.getRandomValue([...namedGearRestrictions])
}

// -- Exotic --

const noExoticGear = new utils.Restriction(4, 2, "no-exotic-gear")
const singleExoticGearPiece = new utils.Restriction(4, 2, "single-exotic-gear-piece")

export const exoticGearRestrictions = [noExoticGear, singleExoticGearPiece]

export function getExoticGearRestrictions() {
    return utils.getRandomValue([...exoticGearRestrictions])
}

